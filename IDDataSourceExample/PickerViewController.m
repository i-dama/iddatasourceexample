//
//  PickerViewController.m
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "PickerViewController.h"
#import "ListWireframe.h"

@interface PickerViewController ()

@end

@implementation PickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - actions

- (IBAction)staticPressed:(id)sender
{
    [ListWireframe presentStaticListOnNavigationController:self.navigationController];
}

- (IBAction)dynamicPressed:(id)sender
{
    [ListWireframe presentDynamicListOnNavigationController:self.navigationController];
}

- (IBAction)persistentPressed:(id)sender
{
    [ListWireframe presentPersistentListOnNavigationController:self.navigationController];
}

@end
