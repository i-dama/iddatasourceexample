//
//  ListViewController.h
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListView.h"

@interface ListViewController : UIViewController <ListView>
@property (weak, nonatomic) id<ListViewDelegate> delegate;
@end
