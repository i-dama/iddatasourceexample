//
//  ListViewController.m
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "ListViewController.h"
#import <IDDataSource/IDTableViewUpdater.h>

@interface ListViewController () <UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) id<IDDataSource> dataSource;
@end

@implementation ListViewController

#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.dataSource numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource numberOfItemsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id item = [self.dataSource itemAtIndexPath:indexPath];
    NSAssert([item conformsToProtocol:@protocol(ListViewItem)], @"ListView only accepts items that conform to ListViewItem protocol");
    id<ListViewItem> listItem = (id<ListViewItem>)item;
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [listItem displayText];
    
    return cell;
}

#pragma mark - View methods

- (void)showLoading
{
    [self.activityIndicator startAnimating];
}

- (void)showErrorWithMessage:(NSString *)message
{
    [self.activityIndicator stopAnimating];
    //Too lazy to implemet this
}

- (void)showDataFromDataSource:(id<IDDataSource>)dataSource
{
    [self.activityIndicator stopAnimating];
    self.dataSource = dataSource;
    [self.tableView reloadData];
    [IDTableViewUpdater updateTableView:self.tableView
                onChangesFromDataSource:dataSource
                           withDelegate:nil];
}

#pragma mark - Actions

- (IBAction)addPressed:(id)sender
{
    [self.delegate didIndicateAddNewItem];
}


@end
