//
//  ListView.h
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IDDataSource/IDDataSource.h>

@protocol ListViewItem <NSObject>

- (NSString *)displayText;

@end

@protocol ListView <NSObject>

- (void)showLoading;
//hide the loading and show the error message
- (void)showErrorWithMessage:(NSString *)message;
//hide the loading and show items from data source
//Items must comform to ListViewItem protocol
- (void)showDataFromDataSource:(id<IDDataSource>)dataSource;

@end

@protocol ListViewDelegate <NSObject>

- (void)didIndicateAddNewItem;

@end