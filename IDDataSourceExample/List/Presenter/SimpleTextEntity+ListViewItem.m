//
//  SimpleTextEntity+ListViewItem.m
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "SimpleTextEntity+ListViewItem.h"

@implementation SimpleTextEntity (ListViewItem)

- (NSString *)displayText
{
    return self.text;
}

@end
