//
//  NSString+ListViewItem.h
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListView.h"

@interface NSString (ListViewItem) <ListViewItem>

@end
