//
//  ListPresenter.h
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListView.h"
#import "ListInteractor.h"

@interface ListPresenter : NSObject <ListViewDelegate>

+ (instancetype)newWithView:(id<ListView>)view interactor:(id<ListInteractor>)interactor;

@end
