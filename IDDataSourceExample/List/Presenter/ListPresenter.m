//
//  ListPresenter.m
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "ListPresenter.h"
#import "NSString+ListViewItem.h"

@interface ListPresenter ()
@property (weak, nonatomic) id<ListView> view;
@property (strong, nonatomic) id<ListInteractor> interactor;
@end

@implementation ListPresenter

+ (instancetype)newWithView:(id<ListView>)view interactor:(id<ListInteractor>)interactor
{
    return [[ListPresenter alloc] initWithView:view interactor:interactor];
}

- (instancetype)initWithView:(id<ListView>)view interactor:(id<ListInteractor>)interactor
{
    self = [super init];
    self.view = view;
    self.interactor = interactor;
    [self loadAndShowData];
    return self;
}

- (void)loadAndShowData
{
    [self.view showLoading];
    __typeof__(self) __weak weakSelf = self;
    [self.interactor loadDataWithSuccess:^(id<IDDataSource> dataSource) {
        if (weakSelf) {
            [weakSelf.view showDataFromDataSource:dataSource];
        }
    } failure:^(NSError *error) {
        if (weakSelf) {
            [weakSelf.view showErrorWithMessage:error.localizedDescription];
        }
    }];
}

#pragma mark - View delegate

- (void)didIndicateAddNewItem
{
    [self.interactor addNewRandomItem];
}

@end
