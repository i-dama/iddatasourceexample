//
//  SimpleTextEntity+ListViewItem.h
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "SimpleTextEntity.h"
#import "ListView.h"

@interface SimpleTextEntity (ListViewItem) <ListViewItem>

@end
