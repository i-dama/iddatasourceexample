//
//  DynamicArrayListInteractor.m
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "DynamicArrayListInteractor.h"
#import <IDDataSource/NSMutableArray+IDDataSource.h>

@interface DynamicArrayListInteractor ()
@property (strong, nonatomic) NSMutableArray *mutableArray;
@end

@implementation DynamicArrayListInteractor

- (void)loadDataWithSuccess:(void (^)(id<IDDataSource>))success failure:(void (^)(NSError *))failure
{
    self.mutableArray = [NSMutableArray arrayWithArray:@[
                                                         @"Only",
                                                         @"A",
                                                         @"Few",
                                                         @"Items"
                                                         ]];
    if (success) {
        success(self.mutableArray);
    }
}

- (void)addNewRandomItem
{
    int random = arc4random() % 10000;
    NSString *randomString = [NSString stringWithFormat:@"%d", random];
    [self.mutableArray ID_addObject:randomString];
}

@end
