//
//  DynamicArrayListInteractor.h
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListInteractor.h"

@interface DynamicArrayListInteractor : NSObject <ListInteractor>

@end
