//
//  PersistentListInteractor.m
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "PersistentListInteractor.h"
#import "SimpleTextEntity.h"
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <IDFetchedDataSource/IDFetchedDataSource.h>

@implementation PersistentListInteractor

- (void)loadDataWithSuccess:(void (^)(id<IDDataSource>))success failure:(void (^)(NSError *))failure
{
    NSFetchedResultsController *fetched = [SimpleTextEntity MR_fetchAllSortedBy:@"text" ascending:YES withPredicate:nil groupBy:@"text" delegate:nil inContext:[NSManagedObjectContext MR_defaultContext]];
    if (fetched) {
        if (success) {
            IDFetchedDataSource *dataSource = [IDFetchedDataSource newWithFetchedResultsController:fetched];
            success(dataSource);
        }
    } else {
        if (failure) {
            failure([NSError errorWithDomain:@"co.infinum.demo" code:123 userInfo:@{NSLocalizedDescriptionKey : @"Failed to instantiate the data source"}]);
        }
    }
    
}

- (void)addNewRandomItem
{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        int random = arc4random() % 10000;
        NSString *randomString = [NSString stringWithFormat:@"%d", random];
        NSDate *date = [NSDate date];
        SimpleTextEntity *newEntity = [SimpleTextEntity MR_createInContext:localContext];
        newEntity.text = randomString;
        newEntity.date = date;
    }];
}

@end
