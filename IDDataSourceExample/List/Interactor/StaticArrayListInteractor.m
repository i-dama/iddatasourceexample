//
//  StaticArrayListInteractor.m
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "StaticArrayListInteractor.h"
#import <IDDataSource/NSArray+IDDataSource.h>

@implementation StaticArrayListInteractor

- (void)loadDataWithSuccess:(void (^)(id<IDDataSource>))success failure:(void (^)(NSError *))failure
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        success(@[
                  @"This",
                  @"Is",
                  @"A",
                  @"Static",
                  @"Array",
                  @"Of",
                  @"Strings",
                  @"Wooohoo"
                  ]);
    });
}

- (void)addNewRandomItem
{
    //do nothing
}

@end
