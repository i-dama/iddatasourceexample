//
//  ListInteractor.h
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IDDataSource/IDDataSource.h>

@protocol ListInteractor <NSObject>

- (void)loadDataWithSuccess:(void (^)(id<IDDataSource> dataSource))success failure:(void (^)(NSError *error))failure;
- (void)addNewRandomItem;

@end
