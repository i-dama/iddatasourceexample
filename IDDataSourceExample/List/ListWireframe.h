//
//  ListWireframe.h
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ListWireframe : NSObject

+ (void)presentStaticListOnNavigationController:(UINavigationController *)navigationController;
+ (void)presentDynamicListOnNavigationController:(UINavigationController *)navigationController;
+ (void)presentPersistentListOnNavigationController:(UINavigationController *)navigationController;

@end
