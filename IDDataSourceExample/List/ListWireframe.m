//
//  ListWireframe.m
//  IDDataSourceExample
//
//  Created by Dama on 29/05/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "ListWireframe.h"
#import <IDObjectLifetime/NSObject+Lifetime.h>

#import "ListPresenter.h"
#import "ListViewController.h"
#import "StaticArrayListInteractor.h"
#import "DynamicArrayListInteractor.h"
#import "PersistentListInteractor.h"

@implementation ListWireframe

+ (void)presentStaticListOnNavigationController:(UINavigationController *)navigationController
{
    [self presentListWithInteractor:[StaticArrayListInteractor new] onNavigationController:navigationController];
}

+ (void)presentDynamicListOnNavigationController:(UINavigationController *)navigationController
{
    [self presentListWithInteractor:[DynamicArrayListInteractor new] onNavigationController:navigationController];
}

+ (void)presentPersistentListOnNavigationController:(UINavigationController *)navigationController
{
    [self presentListWithInteractor:[PersistentListInteractor new] onNavigationController:navigationController];
}

+ (void)presentListWithInteractor:(id<ListInteractor>)interactor onNavigationController:(UINavigationController *)navigationController
{
    UIStoryboard *storyboard = navigationController.storyboard;
    
    ListViewController *view = [storyboard instantiateViewControllerWithIdentifier:@"ListViewController"];
    //preload the view
    [view view];
    ListPresenter *presenter = [ListPresenter newWithView:view interactor:interactor];
    
    view.delegate = presenter;
    
    [NSObject makeObject:presenter haveTheSameLifetimeAsObject:view];
    [navigationController pushViewController:view animated:YES];
}

@end
