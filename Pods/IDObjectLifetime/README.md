# IDObjectLifetime

[![CI Status](http://img.shields.io/travis/Ivan Damjanović/IDObjectLifetime.svg?style=flat)](https://travis-ci.org/Ivan Damjanović/IDObjectLifetime)
[![Version](https://img.shields.io/cocoapods/v/IDObjectLifetime.svg?style=flat)](http://cocoapods.org/pods/IDObjectLifetime)
[![License](https://img.shields.io/cocoapods/l/IDObjectLifetime.svg?style=flat)](http://cocoapods.org/pods/IDObjectLifetime)
[![Platform](https://img.shields.io/cocoapods/p/IDObjectLifetime.svg?style=flat)](http://cocoapods.org/pods/IDObjectLifetime)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

IDObjectLifetime is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "IDObjectLifetime"
```

## Author

Ivan Damjanović, ivan.damjanovic@infinum.hr

## License

IDObjectLifetime is available under the MIT license. See the LICENSE file for more info.
