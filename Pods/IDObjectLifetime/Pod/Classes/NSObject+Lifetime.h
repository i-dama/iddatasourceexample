//
//  NSObject+Lifetime.h
//  Pods
//
//  Created by Dama on 29/05/15.
//
//

#import <Foundation/Foundation.h>

/**
 Basically makes an object 'stick' to another object.
 */
@interface NSObject (Lifetime)

/**
 Makes an object have the same lifetime as the host object.
 When the host object is deallocated, the object will have it's
 retain count reduced by 1.
 */
+ (void)makeObject:(id)anObject haveTheSameLifetimeAsObject:(id)hostObject;

/**
 Reces the retain count of the object hosted on the host object by 1
 */
+ (void)cancelObjectLietimeOnObject:(id)hostObject;

@end
