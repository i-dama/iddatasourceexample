
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// IDDataSource
#define COCOAPODS_POD_AVAILABLE_IDDataSource
#define COCOAPODS_VERSION_MAJOR_IDDataSource 0
#define COCOAPODS_VERSION_MINOR_IDDataSource 1
#define COCOAPODS_VERSION_PATCH_IDDataSource 0

// IDFetchedDataSource
#define COCOAPODS_POD_AVAILABLE_IDFetchedDataSource
#define COCOAPODS_VERSION_MAJOR_IDFetchedDataSource 0
#define COCOAPODS_VERSION_MINOR_IDFetchedDataSource 1
#define COCOAPODS_VERSION_PATCH_IDFetchedDataSource 1

// IDObjectLifetime
#define COCOAPODS_POD_AVAILABLE_IDObjectLifetime
#define COCOAPODS_VERSION_MAJOR_IDObjectLifetime 0
#define COCOAPODS_VERSION_MINOR_IDObjectLifetime 1
#define COCOAPODS_VERSION_PATCH_IDObjectLifetime 0

// MagicalRecord
#define COCOAPODS_POD_AVAILABLE_MagicalRecord
#define COCOAPODS_VERSION_MAJOR_MagicalRecord 2
#define COCOAPODS_VERSION_MINOR_MagicalRecord 2
#define COCOAPODS_VERSION_PATCH_MagicalRecord 0

// MagicalRecord/Core
#define COCOAPODS_POD_AVAILABLE_MagicalRecord_Core
#define COCOAPODS_VERSION_MAJOR_MagicalRecord_Core 2
#define COCOAPODS_VERSION_MINOR_MagicalRecord_Core 2
#define COCOAPODS_VERSION_PATCH_MagicalRecord_Core 0

