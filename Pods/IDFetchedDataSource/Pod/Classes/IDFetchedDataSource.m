//
//  IDFetchedDataSource.m
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import "IDFetchedDataSource.h"

@interface IDFetchedDataSource () <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (copy) IDFetchedDataSourceSectionInfoTransformBlock sectionTransformBlock;
@end

@implementation IDFetchedDataSource
@synthesize delegate;

+ (instancetype)newWithFetchedResultsController:(NSFetchedResultsController *)fetchedResultsController
{
    return [self newWithFetchedResultsController:fetchedResultsController sectionInfoTransformBlock:nil];
}

+ (instancetype)newWithFetchedResultsController:(NSFetchedResultsController *)fetchedResultsController sectionInfoTransformBlock:(IDFetchedDataSourceSectionInfoTransformBlock)transformBlock
{
    IDFetchedDataSource *dataSource = [IDFetchedDataSource new];
    dataSource.fetchedResultsController = fetchedResultsController;
    dataSource.sectionTransformBlock = transformBlock;
	fetchedResultsController.delegate = dataSource;
    return dataSource;
}

#pragma mark - Data source methods

- (NSInteger)numberOfSections
{
    return [self.fetchedResultsController.sections count];
}

- (id)sectionInfoForSection:(NSInteger)sectionIndex
{
    id<NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController.sections objectAtIndex:sectionIndex];
    return [self transformedSectionInfoWithInfo:sectionInfo];
}

- (NSInteger)numberOfItemsInSection:(NSInteger)sectionIndex
{
    id<NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController.sections objectAtIndex:sectionIndex];
    return [sectionInfo numberOfObjects];
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (NSIndexPath *)indexPathForItem:(id)item
{
    return [self.fetchedResultsController indexPathForObject:item];
}

#pragma mark - NSFetchedResultsDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    if ([self.delegate respondsToSelector:@selector(dataSourceWillChangeContent:)]) {
        [self.delegate dataSourceWillChangeContent:self];
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    if ([self.delegate respondsToSelector:@selector(dataSource:didChangeSection:atIndex:forChangeType:)]) {
        [self.delegate dataSource:self didChangeSection:[self transformedSectionInfoWithInfo:sectionInfo] atIndex:sectionIndex forChangeType:[self IDChangeTypeForType:type]];
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    if ([self.delegate respondsToSelector:@selector(controller:didChangeObject:atIndexPath:forChangeType:newIndexPath:)]) {
        [self.delegate dataSource:self didChangeObject:anObject atIndexPath:indexPath forChangeType:[self IDChangeTypeForType:type] newIndexPath:newIndexPath];
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    if ([self.delegate respondsToSelector:@selector(dataSourceDidChangeContent:)]) {
        [self.delegate dataSourceDidChangeContent:self];
    }
}

#pragma mark - Utilities

- (id)transformedSectionInfoWithInfo:(id<NSFetchedResultsSectionInfo>)sectionInfo
{
    return self.sectionTransformBlock ? self.sectionTransformBlock(sectionInfo) : sectionInfo;
}

- (IDDataSourceChangeType)IDChangeTypeForType:(NSFetchedResultsChangeType)type
{
    switch (type) {
        case NSFetchedResultsChangeInsert:
            return IDDataSourceChangeInsert;
        case NSFetchedResultsChangeDelete:
            return IDDataSourceChangeDelete;
        case NSFetchedResultsChangeMove:
            return IDDataSourceChangeMove;
        case NSFetchedResultsChangeUpdate:
            return IDDataSourceChangeUpdate;
    }
}

@end
