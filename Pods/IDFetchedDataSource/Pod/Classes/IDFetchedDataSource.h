//
//  IDFetchedDataSource.h
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import <Foundation/Foundation.h>
#import <IDDataSource/IDDataSource.h>
#import <CoreData/CoreData.h>

typedef id (^IDFetchedDataSourceSectionInfoTransformBlock)(id<NSFetchedResultsSectionInfo> sectionInfo);

@interface IDFetchedDataSource : NSObject <IDDataSource>

/**
 Creates a new instance of the data source with the given NSFetchedResultsController.
 Make sure to perform the fetch on your own
 */
+ (instancetype)newWithFetchedResultsController:(NSFetchedResultsController *)fetchedResultsController;

/**
 Transform block transforms the section info received from the NSFetchedResultsController
 Watch out for the retain cycles in the block
 */
+ (instancetype)newWithFetchedResultsController:(NSFetchedResultsController *)fetchedResultsController sectionInfoTransformBlock:(IDFetchedDataSourceSectionInfoTransformBlock)transformBlock;

@end
