//
//  NSArray+IDDataSource.h
//  Pods
//
//  Created by Dama on 29/05/15.
//
//

#import <Foundation/Foundation.h>
#import "IDDataSource.h"

/**
 Implementation of a IDDataSource using NSArray
 The data source will contain 1 section with the same number of items as the array
 Section info for the section will always be nil
 */
@interface NSArray (IDDataSource) <IDDataSource>

@end
